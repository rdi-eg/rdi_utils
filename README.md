##  Description:
- This is a sample description.
- Another line for the description.
 
## Usage:
> Write preciously how to use this project.

- ### Methods:

> ``` c++
> std::cout << "Hello World" << std::endl;
> ```
> |Description|Parameters|Return|
> |--|--|--|
> | print `hello world` on the screen. |  |  |

> ``` c++
> int add(int num1, int num2);
> ```
> |Description| Paramters | Return |
> |--|--|--|
> | add two numbers.|<ul><li>*num1:* interger number 1</li><li>*num2:* interger number 2.</li></ul>| *int:* return summation<br/>of the two inputs as integer.|

- ### Diagram example:
> ```mermaid
> graph LR
> A[Square Rect] -- Link text --> B((Circle))
> A --> C(Round Rect)
> B --> D{Rhombus}
> C --> D
> ```
## Building:
```sh
cmake -S . -B build/<linux or windows>/<Debug or Release> -DCMAKE_BUILD_TYPE=<Debug or Release>
cmake --build build/<linux or windows>/<Debug or Release> --config <Debug or Release>
```
- if you want to configure/build in parallel add `--parallel <number of cores>`

## Requires:
- read [requires.txt](requires.txt) for dependencies.
