#ifndef RDI_WORD_INFORMATION_H
#define RDI_WORD_INFORMATION_H

#include <string>

namespace RDI
{

#ifndef SWIG
namespace internal
{

struct WordInformation
{
	std::string text;
	float confidence;
	float start;
	float end;
};

} // namespace Internal
#endif // #ifndef SWIG

struct WordInformation
{
	std::wstring text;
    float confidence;
    float start;
    float end;
};

struct WordInformation_v2
{
    std::wstring text;
    float confidence;
    double start;
    double end;
};

}

#endif // RDI_word_information_H
