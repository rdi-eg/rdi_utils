#ifndef PANIC_HPP
#define PANIC_HPP

#include <stdio.h>
#include <stdlib.h>

inline void panic(const char* msg, size_t msg_size, int error_code) {
/// `fwrite`
/// @Param ptr: Pointer to the array of elements to be written, converted to a const void*.
/// @Param size: Size in bytes of each element to be written.
///        size_t is an unsigned integral type.
/// @Param count: Number of elements, each one with a size of size bytes.
///               size_t is an unsigned integral type.
/// @Param stream: Pointer to a FILE object that specifies an output stream. 
	fwrite(msg, msg_size, 1, stderr);
	exit(error_code);
}

#endif // PANIC_HPP
