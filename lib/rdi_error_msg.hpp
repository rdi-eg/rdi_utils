#ifndef RDI_ERROR_MSG_HPP
#define RDI_ERROR_MSG_HPP

#include <string>

#define MODEL_ERROR						std::string("100|model not loaded.")
#define IMAGE_READ_ERROR				std::string("101|image not loaded.")
#define PREPROCESSOR_INTERNAL_ERROR     std::string("102|internal issue in preprocessor.")
#define PREPROCESSOR_RUN_ERROR			std::string("103|the preprocessor did not run properly.")
#define DECODER_INTERNAL_ERROR          std::string("104|internal issue in decoder.")
#define EMPTY_PAGE_ERROR                std::string("105|page is empty.")
#define INTERVALS_ERROR                 std::string("106|invalid intervals.")
#define CONFIG_ERROR                    std::string("107|could not find a value in configuration.")

#define MODEL_ERROR_MSG(MESSAGE)                    MODEL_ERROR                 + " " + (MESSAGE)
#define IMAGE_READ_ERROR_MSG(MESSAGE)               IMAGE_READ_ERROR            + " " + (MESSAGE)
#define PREPROCESSOR_INTERNAL_ERROR_MSG(MESSAGE)    PREPROCESSOR_INTERNAL_ERROR + " " + (MESSAGE)
#define PREPROCESSOR_RUN_ERROR_MSG(MESSAGE)         PREPROCESSOR_RUN_ERROR      + " " + (MESSAGE)
#define DECODER_INTERNAL_ERROR_MSG(MESSAGE)         DECODER_INTERNAL_ERROR      + " " + (MESSAGE)
#define EMPTY_PAGE_ERROR_MSG(MESSAGE)               EMPTY_PAGE_ERROR            + " " + (MESSAGE)
#define INTERVALS_ERROR_MSG(MESSAGE)                INTERVALS_ERROR             + " " + (MESSAGE)
#define CONFIG_ERROR_MSG(MESSAGE)                   CONFIG_ERROR                + " " + (MESSAGE)

#endif // RDI_ERROR_MSG_HPP
