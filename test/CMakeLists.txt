add_executable(test main.cpp tests.cpp rdi_dirty_buckwalter.h)

target_link_libraries(test PRIVATE rdi_utils)

include(${cmake_scripts_path}/rdi_download_unittest_header.cmake)

download_catch(${CMAKE_SOURCE_DIR}/test)
